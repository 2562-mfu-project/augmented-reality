/**
 * Copyright (c) 2017-present, Viro, Inc.
 * All rights reserved.
 *
 * This source code is licensed under the BSD-style license found in the
 * LICENSE file in the root directory of this source tree. An additional grant
 * of patent rights can be found in the PATENTS file in the same directory.
 */

import React, { Component } from 'react';
import { AppRegistry, Image, TouchableOpacity, Text, View, StyleSheet, PixelRatio, TouchableHighlight, ImageBackground } from 'react-native';
import Swiper from 'react-native-swiper';
import { ViroVRSceneNavigator, ViroARSceneNavigator } from 'react-viro';

/*
 TODO: Insert your API key below
 */
var sharedProps = {
    apiKey: 'API_KEY_HERE',
};

// Sets the default scene you want for AR and VR
var InitialARScene = require('./js/HelloWorldSceneAR');
var ScanTreeScreen = require('./src/screens/ScanTreeScreen');

var UNSET = 'UNSET';
var VR_NAVIGATOR_TYPE = 'VR';
var AR_NAVIGATOR_TYPE = 'AR';

// This determines which type of experience to launch in, or UNSET, if the user should
// be presented with a choice of AR or VR. By default, we offer the user a choice.
var defaultNavigatorType = UNSET;

export default class ViroSample extends Component {
    constructor() {
        super();

        this.state = {
            navigatorType: defaultNavigatorType,
            sharedProps: sharedProps,
        };
        this._getExperienceSelector = this._getExperienceSelector.bind(this);
        this._getARNavigator = this._getARNavigator.bind(this);
        this._getVRNavigator = this._getVRNavigator.bind(this);
        this._getExperienceButtonOnPress = this._getExperienceButtonOnPress.bind(this);
        this._exitViro = this._exitViro.bind(this);
    }

    // Replace this function with the contents of _getVRNavigator() or _getARNavigator()
    // if you are building a specific type of experience.
    render() {
        if (this.state.navigatorType == UNSET) {
            return this._getExperienceSelector();
        } else if (this.state.navigatorType == VR_NAVIGATOR_TYPE) {
            return this._getVRNavigator();
        } else if (this.state.navigatorType == AR_NAVIGATOR_TYPE) {
            return this._getARNavigator();
        }
    }

    // Presents the user with a choice of an AR or VR experience
    _getExperienceSelector() {
        return (
            <View style={{ flex: 1 }}>
                <View style={{ width: '100%', height: 120, backgroundColor: 'white' }}>
                    <Image source={require('./src/icons/top-gradient.png')} style={{ position: 'absolute', resizeMode: 'cover' }}></Image>
                </View>
                <View style={{ flex: 1, alignItems: 'center', justifyContent: 'center', backgroundColor: 'lightgray' }}>
                    <View style={{ width: 400, height: 300, backgroundColor: 'gray' }}>
                        <Swiper showsButtons={true}>
                            <View style={styles.slide1}>
                                <Image source={require('./src/images/general/seed.jpg')} style={{ width: '100%' }}></Image>
                            </View>
                            <View style={styles.slide2}>
                                <Image source={require('./src/images/general/seed.jpg')} style={{ width: '100%' }}></Image>
                            </View>
                            <View style={styles.slide3}>
                                <Image source={require('./src/images/general/seed.jpg')} style={{ width: '100%' }}></Image>
                            </View>
                        </Swiper>
                    </View>
                </View>

                <ImageBackground style={{ width: '100%', height: 80, backgroundColor: 'white' }} source={require('./src/icons/bottom-gradient.png')}>
                    <TouchableHighlight style={styles.camera_button} onPress={this._getExperienceButtonOnPress(VR_NAVIGATOR_TYPE)} underlayColor={'#68a0ff'}>
                        <Text>Scan</Text>
                    </TouchableHighlight>
                </ImageBackground>
            </View>
        );
    }

    // Returns the ViroARSceneNavigator which will start the AR experience
    _getARNavigator() {
        return <ViroARSceneNavigator {...this.state.sharedProps} initialScene={{ scene: InitialARScene }} />;
    }

    // Returns the ViroSceneNavigator which will start the VR experience
    _getVRNavigator() {
        return (
            <View style={{ flex: 1 }}>
                <View style={{ position: 'absolute', zIndex: 10, width: '100%', height: 80, backgroundColor: 'rgba(0,0,0,0)', flexDirection: 'row' }}>
                    <TouchableOpacity onPress={this._getExperienceButtonOnPress(UNSET)} style={{ flex: 1, alignItems: 'center', justifyContent: 'center', backgroundColor: 'gray' }}>
                        <Image source={require('./src/icons/icon-arrow-left.png')}></Image>
                    </TouchableOpacity>
                    <View style={{ flex: 10, alignItems: 'center', justifyContent: 'center', backgroundColor: 'rgba(0,0,0,0)' }}></View>
                    <View style={{ flex: 1, alignItems: 'center', justifyContent: 'center', backgroundColor: 'rgba(0,0,0,0)' }}></View>
                </View>
                <View style={{ flex: 1, backgroundColor: 'gray' }}>
                    <ViroARSceneNavigator {...this.state.sharedProps} initialScene={{ scene: ScanTreeScreen }} onExitViro={this._exitViro} />
                </View>
            </View>
        );
    }

    // This function returns an anonymous/lambda function to be used
    // by the experience selector buttons
    _getExperienceButtonOnPress(navigatorType) {
        return () => {
            this.setState({
                navigatorType: navigatorType,
            });
        };
    }

    // This function "exits" Viro by setting the navigatorType to UNSET.
    _exitViro() {
        this.setState({
            navigatorType: UNSET,
        });
    }
}

var styles = StyleSheet.create({
    camera_button: {
        alignSelf: 'center',
        alignItems: 'center',
        justifyContent: 'center',
        width: 80,
        height: 80,
        borderRadius: 40,
        top: -40,
        backgroundColor: 'lightgray',
        shadowColor: '#000',
        shadowOffset: { width: 0, height: 2 },
        shadowOpacity: 0.25,
        shadowRadius: 3.84,

        elevation: 5,
    },
    wrapper: {},
    slide1: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: '#9DD6EB',
    },
    slide2: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: '#97CAE5',
    },
    slide3: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: '#92BBD9',
    },
    text: {
        color: '#fff',
        fontSize: 30,
        fontWeight: 'bold',
    },
});

var localStyles = StyleSheet.create({});
