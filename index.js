import { AppRegistry } from 'react-native';
import App from './App.js';

AppRegistry.registerComponent('AR-MFU', () => App);

// The below line is necessary for use with the TestBed App
AppRegistry.registerComponent('ViroSample', () => App);
